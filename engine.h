#pragma once
#include <vector>
#include "tank.h"

class engine
{
public:
	engine();
	void start_engine();
	void stop_engine();
	void give_back_fuel(double);
	void connect_fuel_tank_to_engine(tank);
	void disconnect_fuel_tank_from_engine(tank);
	bool get_connection();
private:
	const double fuel_per_second = 5.5;
	bool status;
	tank internal_tank;
	bool connection;
	int noOf_connected_tanks; //number of connected tanks
	std::vector<tank> connected_tanks;
};

