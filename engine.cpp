#include "engine.h"
#include "tank.h"
using namespace std;

engine::engine() {
	status = 0;
	internal_tank.set_capacity(55);
	noOf_connected_tanks = 0;
}
void engine::start_engine()
{
	if (get_connection() == 1)
	{
		status = true;
	}
	else
	{
		status = false;
	}
   
}

void engine::stop_engine()
{
    status = false;
}

void engine::give_back_fuel(double gbf)
{
	double tmp;
	tmp = internal_tank.get_fuel_quantity();
	tmp = tmp - gbf;
	internal_tank.set_fuel_quantity(tmp);
}

void engine::connect_fuel_tank_to_engine(tank t)
{
	noOf_connected_tanks++;
	connected_tanks.push_back(t);
}

void engine::disconnect_fuel_tank_from_engine(tank t)
{
	noOf_connected_tanks--;
	connected_tanks.erase(connected_tanks.begin() + noOf_connected_tanks);
}

bool engine::get_connection() {
	if (noOf_connected_tanks == 0) return false;
	else return true;
}