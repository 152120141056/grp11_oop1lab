#include "simulation.h"

simulation::simulation()
{
	int noOf_tanks = 0;
}

tank simulation::add_fuel_tank(double capacity)
{
	noOf_tanks++;
	tank t;
	t.set_capacity(capacity);
	t.set_tank_id(noOf_tanks);
	tanks.push_back(t);
	return t;
}

void simulation::list_fuel_tank()
{

}

void simulation::remove_fuel_tank(tank t)
{
	noOf_tanks--;
	tanks.erase(tanks.begin() + noOf_tanks);
}

void simulation::break_fuel_tank(tank t)
{
	t.set_broken(1);
}

void simulation::repair_fuel_tank(tank t)
{
	t.set_broken(0);
}

void simulation::fill_tank(tank t, double quantity)
{
	if ((t.get_fuel_quantity() + quantity) > t.get_capacity())  t.set_fuel_quantity(t.get_capacity()); //fuel_quantity = capacity
	else  t.set_fuel_quantity(t.get_fuel_quantity() + quantity);  //fuel_quantity += quantity;
}


void simulation::open_valve(tank t)
{
	t.set_valve_status(1);
}

void simulation::close_valve(tank t)
{
	t.set_valve_status(0);
}