#pragma once

class tank
{
public:
	tank();
	void set_broken(bool);
	bool get_broken();
	void set_fuel_quantity(double);
	double get_fuel_quantity();
	void set_capacity(double);
	double get_capacity();
	void set_tank_id(int);
	int get_tank_id();
	void set_valve_status(bool);
	bool get_valve_status();
private:
	bool broken;
	double capacity;
	double fuel_quantity;
	int tank_id;
	bool valve_status;
};
