#include "engine.h"
#include "tank.h"
#include "valve.h"

#include <iostream>
#include <fstream>
#include <string>
#include "input.h"

using namespace std;

void input::openFile(string input, string output)
{
	string command;
	engine e;
	tank t;
	valve v;
	ifstream readfile;
	readfile.open(input);
	if (readfile.is_open())
	{
		while (getline(readfile, command))
		{
			if (command == "start_engine") e.start_engine();
			else if (command == "stop_engine") e.stop_engine();
			else if (command == "absorb_fuel")e.adsorb_fuel(0);
			else if (command == "give_back_fuel")e.give_back_fuel(0);
			else if (command == "add_fuel_tank")t.add_fuel_tank(0);
			else if (command == "list_fuel_tanks")t.list_fuel_tank();
			else if (command == "remove_fuel_tank")t.remove_fuel_tank(0);
			else if (command == "connect_fuel_tank_to_engine")t.connect_fuel_tank_to_engine(0);
			else if (command == "disconnect_fuel_tank_from_engine")t.disconnect_fuel_tank_from_engine(0);
			else if (command == "open_valve")v.open_valve(t);
			else if (command == "close_valve")v.close_valve(t);
			else if (command == "break_fuel_tank")t.break_fuel_tank(0);
			else if (command == "repair_fuel_tank")t.repair_fuel_tank(0);
			else if (command == "stop_simulation") ;
			else cout << "Wrong command!" << endl;
		}

		readfile.close();
	}

	else cout << "Unable to open file";
}