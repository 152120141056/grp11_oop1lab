#pragma once
#include "engine.h"
#include "tank.h"
#include <vector>

class simulation {
public:
	simulation();
	tank add_fuel_tank(double);
	void list_fuel_tank();
	void remove_fuel_tank(tank);
	void break_fuel_tank(tank);
	void repair_fuel_tank(tank);
	void fill_tank(tank t, double quantity);
	void open_valve(tank);
	void close_valve(tank);
private:
	int noOf_tanks;
	std::vector<tank> tanks;
};
