#include "tank.h"
#include "engine.h"

tank::tank() {
	capacity = 0;
	fuel_quantity = 0;
	broken = 0;
}

void tank::set_broken(bool b) {
	broken = b;
}

bool tank::get_broken() {
	return broken;
}

void tank::set_capacity(double cap) {
	capacity = cap;
}

double tank::get_capacity() {
	return capacity;
}

void tank::set_tank_id(int id) {
	tank_id = id;
}

int tank::get_tank_id() {
	return tank_id;
}

void tank::set_fuel_quantity(double fq) {
	fuel_quantity = fq;
}

double tank::get_fuel_quantity() {
	return fuel_quantity;
}

void tank::set_valve_status(bool v)
{
	valve_status = v;
}

bool tank::get_valve_status()
{
	return valve_status;
}